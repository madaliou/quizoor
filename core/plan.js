exports.getPlan = () => {
	const plan = {
		id: "plan_FV5WiXhmVPcTEf",
		object: "plan",
		active: true,
		aggregate_usage: null,
		amount: 20,
		billing_scheme: "per_unit",
		created: 1564054943,
		currency: "usd",
		interval: "month",
		interval_count: 1,
		livemode: true,
		metadata: {},
		nickname: "",
		product: "prod_FV5WWnjqCvMmCP",
		tiers: null,
		tiers_mode: null,
		transform_usage: null,
		trial_period_days: null,
		usage_type: "licensed",
	};

	return plan;
};

exports.getTestPlan = () => {
	const plan = {
		id: "plan_GUApcD2I8pp2D1",
		object: "plan",
		active: true,
		aggregate_usage: null,
		amount: 2000,
		amount_decimal: "2000",
		billing_scheme: "per_unit",
		created: 1578143748,
		currency: "usd",
		interval: "month",
		interval_count: 1,
		livemode: false,
		metadata: {},
		nickname: "",
		product: "prod_GUAmFBX5IamLji",
		tiers: null,
		tiers_mode: null,
		transform_usage: null,
		trial_period_days: null,
		usage_type: "licensed",
	};

	return plan;
};
