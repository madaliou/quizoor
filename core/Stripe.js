//const stripe = require("stripe")("sk_live_ibTqVEZXAfwHkBsy3rEoRplE")
const stripe = require("stripe")(process.env.STRIPE_DEV_SECRET_KEY);
const plan = require("./plan").getPlan().id;
const Testplan = require("./plan").getTestPlan().id;

module.exports = class StripeManager {
	constructor() {}

	async createPaymentMethod(data) {
		const paymentMethod = await stripe.paymentMethods.create({
			type: "card",
			card: data.card,
			billing_details: {
				email: data.email,
				name: data.firstName + " " + data.lastName,
				address: data.address,
				phone: data.phoneNumber,
			},
		});
		return paymentMethod;
	}

	async createCustomer(data) {
		const customer = await stripe.customers.create({
			email: data.email,
			payment_method: data.payment_method,
			invoice_settings: {
				default_payment_method: data.payment_method,
			},
		});
		return customer;
	}

	async attachNewPaymentMethod(customer, payment_method) {
		const newPaymentMethod = await stripe.paymentMethods.attach(payment_method, {
			customer: customer,
		});
		return newPaymentMethod;
	}

	async setDefaultPaymentMethod(customer, payment_method) {
		const defaultPaymentMethod = await stripe.customers.update(customer, {
			invoice_settings: {
				default_payment_method: payment_method,
			},
		});
		return defaultPaymentMethod;
	}

	async createCardToken(card) {
		const token = await stripe.tokens.create({
			card: card,
		});
		return token;
	}

	async updateCard(data) {
		const customer = await stripe.customers.update(data.customer, {
			source: data.token,
		});
		return customer;
	}

	async createSubscription(customer) {
		const subscription = await stripe.subscriptions.create({
			customer: customer,
			collection_method: "charge_automatically",
			items: [
				{
					plan: Testplan,
				},
			],
			trial_period_days: 7,
		});
		return subscription;
	}

	async createSubscriptionWithoutTrialDays(customer) {
		const subscription = await stripe.subscriptions.create({
			customer: customer,
			collection_method: "charge_automatically",
			items: [
				{
					plan: Testplan,
				},
			],
		});
		return subscription;
	}

	async createCharge(data) {
		const charge = await stripe.charges.create({
			amount: data.amount * 100,
			currency: "usd",
			customer: data.customer,
			source: data.source, //Use only source for default payment on live website
			receipt_email: data.email,
			description: data.description,
		});
		return charge;
	}

	async createTranslationCharge(data) {
		const charge = await stripe.charges.create({
			amount: data.amount * 100,
			currency: "usd",
			source: data.source, //Use only source for default payment on live website
			receipt_email: data.email,
			description: data.description,
		});
		return charge;
	}

	async createCoupon() {
		const coupon = await stripe.coupons.create({
			id: "twenty-percent-discount",
			duration: "once",
			percent_off: 20,
		});
		return coupon;
	}

	async findCoupon() {
		const coupon = await stripe.coupons.retrieve("twenty-percent-discount");
		return coupon;
	}

	async applyDiscount(_id) {
		const discount = await stripe.subscriptions.update(_id, {
			coupon: "twenty-percent-discount",
		});
		return discount;
	}

	async removeDiscount(_id) {
		const discount = await stripe.subscriptions.update(_id, {
			coupon: null,
		});
		return discount;
	}

	async createDiscountSubscription(customer, coupon) {
		const subscription = await stripe.subscriptions.create({
			customer: customer,
			collection_method: "charge_automatically",
			items: [
				{
					plan: Testplan,
				},
			],
			coupon: "twenty-percent-discount",
			trial_period_days: 7,
		});
		return subscription;
	}

	async retrieveSubscription(_id) {
		const subscription = await stripe.subscriptions.retrieve(_id);
		return subscription;
	}

	async deleteSubscription(_id) {
		const subscription = await stripe.subscriptions.update(_id, {
			cancel_at_period_end: true,
		});
		return subscription;
	}

	async detachPaymentMethod(payment_method) {
		const paymentMethodDetached = await stripe.paymentMethods.detach(payment_method);
		return paymentMethodDetached;
	}
};