const User = require('./User');

let user = {
	firstname: 'admin',
	lastname: 'quizoor',
	email: 'quizoor@gmail.com',
	password:'test1234',
	status: 'activated',
	role: 'superadmin',
	locations: [
		{ text: 'English', value: 'en' },
		{ text: 'French', value: 'fr' },
		{ text: 'German', value: 'de' },
		{ text: 'Portugese', value: 'pt' }
	]
};

const seeder = async () => {
	try {
		const _user = await User.findOne({ firstname: 'admin', lastname: 'quizoor', role: 'superadmin' });
		if (_user) {
			console.log('Admin already exist');
		} else {
			const _admin = new User(user);
			await _admin.save();
			console.log(`Admin created successfully : ${_admin}`);
		}
	} catch (error) {
		console.log(`An error occurred : ${error.message}`);
	}
};

module.exports = seeder;
