const mongoose = require("mongoose");

const SubscriptionSchema = mongoose.Schema({
	user: {
		type: mongoose.Schema.Types.ObjectId,
		ref: "User",
		required: [true, "Please set the user"],
	},

	amount: {
		type: Number,
		required: [true, "Please set the amount"],
		default: 20,
	},

	start: {
		type: Date,
		required: [true, "Please set the starting date"],
	},

	end: {
		type: Date,
		required: [true, "Please set the ending date"],
	},

	trialStart: {
		type: Date,
	},

	trialEnd: {
		type: Date,
	},
	status: {
		type: String,
		required: [true, "Please set the status"],
	},

	createdAt: {
		type: Date,
		default: Date.now,
	},
});

module.exports = mongoose.model("Subscription", SubscriptionSchema);
