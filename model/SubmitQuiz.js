const mongoose = require('mongoose');
// const User = require('./User');
const Schema = mongoose.Schema;
const submitQuizSchema = Schema({
    /* name: {
        type: String,
        required: true
    }, */
    status: {
        type: String,
        default: 'activated'
    },
    user: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'User'
    },
    subject: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'Subject'
    },
    chapter: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: 'Chapter'
    },
    filename: {
        type: String,
        //required: true
    },
    /* location: {
        type: String,
        required: true
    }, */
    create_date: { type:Date, default:Date.now }
});

const SubmitQuiz = mongoose.model("SubmitQuiz", submitQuizSchema);
module.exports = SubmitQuiz;