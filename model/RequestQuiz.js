const mongoose = require('mongoose');
// const User = require('./User');
const Schema = mongoose.Schema;
const requestQuizSchema = Schema({
    status: {
        type: String,
        default: 'activated'
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    numberOfQuestions: {
        type: Number,
        required: true
    },
   
    filename: {
        type: String,
        //required: true
    },
    secondFilename: {
        type: String,
        //required: true
    },
    
    create_date: { type:Date, default:Date.now }
});

const RequestQuiz = mongoose.model("RequestQuiz", requestQuizSchema);
module.exports = RequestQuiz;