const dotenv = require('dotenv')
dotenv.config()
const express = require("express");
const PORT = process.env.PORT || 4000;
const morgan = require("morgan");
const cors = require("cors");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const config = require("./config/db");
const app = express();
const seeder= require('./model/database-initializer');

//configure database and mongoose

mongoose.set("useCreateIndex", true);
mongoose
  .connect(config.database, { useNewUrlParser: true })
  .then(async() => {
    console.log("Database is connected");
    const seed = await seeder()
    console.log('seeder :',seed)
  })
  .catch(err => {
    console.log({ database_error: err });
  });


// db configuaration ends here
//registering cors
app.use(cors());
//configure body parser
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(bodyParser.json({ limit: '50mb', extended: true }));
//configure body-parser ends here
app.use(morgan("dev")); // configire morgan
// define first route
app.get("/", (req, res) => {
  console.log("Hello MEVN Soldier");
});

const routes = require("./routes/router"); //bring in our user routes
app.use("/", routes);
app.listen(PORT, () => {
  console.log(`App is running on ${PORT}`);
});

