const chalk = require("chalk");
const User = require("../../model/User");
const Level = require("../../model/Level");
const Category = require("../../model/Catetory");
const Subject = require("../../model/Subject");
const Chapter = require("../../model/Chapter");
const Quiz = require("../../model/Quiz");
const QuizResult = require("../../model/QuizResult");

exports.getSearchValue = async (req, res) => {
	try {
		const location = req.params.id;
		const levels = await Level.find({ location: location, status: "activated" });
		console.log(levels);
		return res.status(200).json(levels);
	} catch (error) {
		return res.status(404).json({ message: error });
	}
};
exports.getSearchCategory = async (req, res) => {
	try {
		const levelID = req.params.id;
		const categories = await Category.find({ level: levelID, status: "activated" });
		console.log(categories);
		return res.status(200).json(categories);
	} catch (error) {
		return res.status(400).json({ message: error });
	}
};
exports.getSearchSubject = async (req, res) => {
	try {
		const categoryID = req.params.id;
		const subjects = await Subject.find({ category: categoryID, status: "activated" });
		console.log(subjects);
		return res.status(200).json(subjects);
	} catch (error) {
		return res.status(400).json({ message: error });
	}
};

exports.getSubjects = async (req, res) => {
	try {
		const location = req.params.id;
		const levels = await Level.find({ location: location });
		const tempLevelIds = [];
		levels.map(function (level) {
			tempLevelIds.push(level._id);
		});
		const levelIDs = tempLevelIds;
		const subjectItems = await Subject.find({ level: { $in: levelIDs }, status: "activated" })
			.populate("level")
			.populate("category");
		return res.status(200).json(subjectItems);
	} catch (error) {}
};

exports.getSearchTrakItems = async (req, res) => {
	try {
		const user = req.body.user;
		const subjectId = req.body.id;
		const quizHistories = await QuizResult.find({ user: user });
		const subjectInformation = await Subject.findOne({ _id: subjectId, status: "activated" })
			.populate("level")
			.populate("category");
		const chapterItems = await Chapter.find({ subject: subjectId, status: "activated" })
			.populate("level")
			.populate("category")
			.populate("subject");
		if (chapterItems.length > 0) {
			const tquizItems = await Quiz.find({ chapter: chapterItems[0]._id, verification: "checked", status: "activated" })
				.sort({ name: 1 })
				.populate("chapter")
				.populate("user");
			if (tquizItems.length > 0) {
				let tempItems = [];
				for (let i = 0; i < tquizItems.length; i++) {
					let checkPassed = 0;
					let guessResult = "";
					let totalMark = "";
					let timePassed = 0;
					for (let j = 0; j < quizHistories.length; j++) {
						if (JSON.stringify(tquizItems[i]._id) == JSON.stringify(quizHistories[j].quiz)) {
							checkPassed++;
							guessResult = quizHistories[j].guessResult;
							totalMark = quizHistories[j].totalMark;
							timePassed = quizHistories[j].timePassed;
						}
					}
					if (checkPassed > 0) {
						tempItems.push({
							_id: tquizItems[i]._id,
							name: tquizItems[i].name,
							difficulty: tquizItems[i].difficulty,
							author: tquizItems[i].user.firstname + " " + tquizItems[i].user.lastname,
							completed: true,
							questionNumbers: tquizItems[i].questions.length,
							guessResult: guessResult,
							totalMark: totalMark,
							timePassed: timePassed,
							chapterName: tquizItems[i].chapter.name,
							chapterContent: tquizItems[i].chapter.content,
						});
					} else if (checkPassed == 0) {
						tempItems.push({
							_id: tquizItems[i]._id,
							name: tquizItems[i].name,
							difficulty: tquizItems[i].difficulty,
							author: tquizItems[i].user.firstname + " " + tquizItems[i].user.lastname,
							completed: false,
							questionNumbers: tquizItems[i].questions.length,
							guessResult: 0,
							totalMark: 0,
							timePassed: timePassed,
							chapterName: tquizItems[i].chapter.name,
							chapterContent: tquizItems[i].chapter.content,
						});
					}
				}
				let quizItems = tempItems;
				let sortedItems = quizItems.sort((a, b) => {
					var nameA = parseInt(a.name.substring(a.name.search(" "), a.name.length)),
						nameB = parseInt(b.name.substring(b.name.search(" "), b.name.length));
					if (nameA < nameB) {
						//sort string ascending
						
						return -1;
					}

					if (nameA > nameB) {
						
						return 1;
					}
					//return 0; //default return value (no sorting)
				});
				console.log(sortedItems);
				let resData = {
					subjectInformation: subjectInformation,
					chapterItems: chapterItems,
					quizItems: sortedItems,
				};
				//console.log(quizItems);
				return res.status(200).json(resData);
			} else if (tquizItems.length == 0) {
				let resData = {
					subjectInformation: subjectInformation,
					chapterItems: chapterItems,
					quizItems: [],
				};
				return res.status(200).json(resData);
			}
		} else if (chapterItems.length == 0) {
			let resData = {
				subjectInformation: subjectInformation,
				chapterItems: [],
				quizItems: [],
			};
			return res.status(200).json(resData);
		}
	} catch (error) {
		return res.status(404).json(error);
	}
};

exports.updateQuizItem = async (req, res) => {
	try {
		const user = req.body.user;
		const chapterId = req.body.id;
		const quizHistories = await QuizResult.find({ user: user });
		const chapterItems = await Chapter.findOne({ _id: chapterId });
		const tquizItems = await Quiz.find({ chapter: chapterId, verification: "checked", status: "activated" })
			.populate("chapter")
			.populate("user");
		let tempItems = [];
		for (let i = 0; i < tquizItems.length; i++) {
			let checkPassed = 0;
			let guessResult = "";
			let totalMark = "";
			let timePassed = 0;
			for (let j = 0; j < quizHistories.length; j++) {
				if (JSON.stringify(tquizItems[i]._id) == JSON.stringify(quizHistories[j].quiz)) {
					checkPassed++;
					guessResult = quizHistories[j].guessResult;
					totalMark = quizHistories[j].totalMark;
					timePassed = quizHistories[j].timePassed;
				}
			}
			if (checkPassed > 0) {
				tempItems.push({
					_id: tquizItems[i]._id,
					name: tquizItems[i].name,
					difficulty: tquizItems[i].difficulty,
					author: tquizItems[i].user.firstname + " " + tquizItems[i].user.lastname,
					completed: true,
					questionNumbers: tquizItems[i].questions.length,
					guessResult: guessResult,
					totalMark: totalMark,
					timePassed: timePassed,
					chapterName: tquizItems[i].chapter.name,
					chapterContent: tquizItems[i].chapter.content,
				});
			} else if (checkPassed == 0) {
				tempItems.push({
					_id: tquizItems[i]._id,
					name: tquizItems[i].name,
					difficulty: tquizItems[i].difficulty,
					author: tquizItems[i].user.firstname + " " + tquizItems[i].user.lastname,
					completed: false,
					questionNumbers: tquizItems[i].questions.length,
					guessResult: 0,
					totalMark: 0,
					timePassed: timePassed,
					chapterName: tquizItems[i].chapter.name,
					chapterContent: tquizItems[i].chapter.content,
				});
			}
		}
		let quizItems = tempItems;
		let sortedItems = quizItems.sort((a, b) => {
			//substring quizz for string and convert to int the number at the end of the string
			var nameA = parseInt(a.name.substring(a.name.search(" "), a.name.length)),
				nameB = parseInt(b.name.substring(b.name.search(" "), b.name.length));
			if (nameA < nameB)
				//sort string ascending
				return -1;
			if (nameA > nameB) return 1;
			return 0; //default return value (no sorting)
		});
		let resData = {
			chapterItems: chapterItems,
			quizItems: sortedItems,
		};
		//console.log(quizItems);
		return res.status(200).json(resData);
	} catch (error) {
		console.log(error);
		return res.status(404).json(error);
	}
};

exports.getTestItem = async (req, res) => {
	try {
		const quizId = req.params.id;
		const getTestItems = await Quiz.findOne({ _id: quizId, status: "activated" });
		return res.status(200).json(getTestItems);
	} catch (error) {
		return res.status(404).json(error);
	}
};

exports.saveTestResult = async (req, res) => {
	//console.log('suis dans la function');
	let user = req.body.userID;
	let quiz = req.body.quizID;
	let totalMark = req.body.totalMark;
	let guessResult = req.body.guessResult;
	let checkTestResult = await QuizResult.findOne({ quiz: quiz, user: user });
	if (checkTestResult) {
		//console.log('suis dans le if de save result');
		checkTestResult.totalMark = totalMark;
		checkTestResult.guessResult = guessResult;
		checkTestResult.timePassed = checkTestResult.timePassed + 1;
		console.log(checkTestResult.timePassed);
		checkTestResult
			.save()
			.then((quizResult) => {
				return res.status(200).json({ message: "Result saved Successfully" });
			})
			.catch((error) => {
				return res.status(400).json({ message: error });
			});
	} else {
		console.log("suis dans le else de save result");
		let quizResult = new QuizResult({
			user: user,
			quiz: quiz,
			totalMark: totalMark,
			guessResult: guessResult,
			timePassed: 1,
		});
		quizResult
			.save()
			.then((quizResult) => {
				return res.status(200).json({ message: "Result saved Successfully" });
			})
			.catch((error) => {
				return res.status(400).json({ message: error });
			});
	}
};

exports.getAllSubjectItems = async (req, res) => {
	console.log(req.params.id);
	let locale = req.params.id;
	let levels = await Level.find({ location: locale });
	let templevelIds = [];
	levels.map(function (level) {
		templevelIds.push(level._id);
	});
	let levelIds = templevelIds;
	Subject.find({ level: { $in: levelIds } })
		.then((subject) => {
			return res.status(200).json(subject);
		})
		.catch((error) => {
			return res.status(400).json({ message: error });
		});
	console.log(levels);
};

exports.searchSubject = async (req, res) => {
	let search = req.body.search;
	let locale = req.body.locale;
	let levels = await Level.find({ location: locale });
	let templevelIds = [];
	levels.map(function (level) {
		templevelIds.push(level._id);
	});
	let LevelIds = templevelIds;
	let subjects = await Subject.find({ name: { $regex: search, $options: "i" }, level: { $in: LevelIds } });
	if (subjects) {
		return res.status(200).json(subjects);
	} else {
		return res.status(201).json({ message: "Item does not exist" });
	}
};
