const SubmitQuiz = require('../../model/SubmitQuiz');
const RequestQuiz = require('../../model/RequestQuiz');


exports.getSubmitQuiz = async (req, res) => {
	SubmitQuiz.find()
	.populate("user")
	.populate("subject")
	.populate("chapter")
	.then((submitQuiz) => {
		return res.status(200).json(submitQuiz);
	})
	.catch(error => {
		return res.status(400).json(error);
	});
	
};

exports.addSubmitQuiz = async (req, res) => {
    console.log("req.body : ", req.body);
    let submitQuiz = new SubmitQuiz();
   
	submitQuiz.subject = req.body.subject;
	submitQuiz.user = req.userData._id;
	submitQuiz.chapter = req.body.chapter;
	submitQuiz.filename = req.file.filename;
	submitQuiz.save();
	return res.status(201).json({ message: "SubmitQuiz uploaded successfully" });
	
};

exports.updateSubmitQuiz = async (req, res) => {

	const id = req.params.id;
	let {subject, chapter} = req.body;

	console.log('subject : ', subject, '  chapter : ', chapter);
	console.log('req.body : ', req.body);

	SubmitQuiz.findByIdAndUpdate(id, { subject, chapter }).then(
		submitQuiz => {
			return res.status(200).json({message: 'Submit quizz successfully updated', submitQuiz})
		}
	).catch(
		error => {
			return res.status(500).json({message: error})
		}
	)
	
};


exports.deleteSubmitQuiz = async (req, res) => {

	const id = req.params.id;
	SubmitQuiz.deleteOne({ _id: id })
		.then(
			(resp) => {
				return res.status(200).json({ message: "The Item successfully deleted" });
			},
			(err) => {
				console.log('err : ', err);
				return res.status(400).json({ message: err });
			}
		)
		.catch((error) => {
			console.log('error : ', error);
			return res.status(400).json({ message: error });
		});

};


exports.getRequestQuiz = async (req, res) => {
	RequestQuiz.find()
	.populate("user")
	.then((requestQuiz) => {
		return res.status(200).json(requestQuiz);
	})
	.catch(error => {
		return res.status(400).json(error);
	});
	
};


exports.addRequestQuiz = async (req, res) => {
	console.log("req.body : ", req.body);
	console.log("req.file : ", req.files);
	let files = req.files;
	let requestQuiz = new RequestQuiz();
	requestQuiz.numberOfQuestions = req.body.numberOfQuestions;
	/* req.files.forEach(file => {
		
	}); */

	if(files.length === 2){
		requestQuiz.filename = files[0].filename;
		requestQuiz.secondFilename = files[1].filename;
	}else if(files.length === 1){
		requestQuiz.filename = files[0].filename;
	}
	
	requestQuiz.save();
	return res.status(201).json({ message: "RequestQuiz uploaded successfully" });
	
};

exports.updateRequestQuiz = async (req, res) => {

	const id = req.params.id;
	let {numberOfQuestions} = req.body;

	if(files.length === 2){
		let filename = files[0].filename;
		let secondFilename = files[1].filename;
		RequestQuiz.findByIdAndUpdate(id, { numberOfQuestions, filename, secondFilename }).then(
			requestQuiz => {
				return res.status(200).json({message: 'Submit quizz successfully updated', requestQuiz})
			}
		).catch(
			error => {
				return res.status(500).json({message: error})
			}
		)
	}else if(files.length === 1){
		let filename = files[0].filename;
		RequestQuiz.findByIdAndUpdate(id, { numberOfQuestions, filename }).then(
			requestQuiz => {
				return res.status(200).json({message: 'Submit quizz successfully updated', requestQuiz})
			}
		).catch(
			error => {
				return res.status(500).json({message: error})
			}
		)

	}else {
		RequestQuiz.findByIdAndUpdate(id, { numberOfQuestions }).then(
			requestQuiz => {
				return res.status(200).json({message: 'Submit quizz successfully updated', requestQuiz})
			}
		).catch(
			error => {
				return res.status(500).json({message: error})
			}
		)
	}
	
};


exports.deleteRequestQuiz = async (req, res) => {

	const id = req.params.id;
	RequestQuiz.deleteOne({ _id: id })
		.then(
			(resp) => {
				return res.status(200).json({ message: "The Item successfully deleted" });
			},
			(err) => {
				console.log('err : ', err);
				return res.status(400).json({ message: err });
			}
		)
		.catch((error) => {
			console.log('error : ', error);
			return res.status(400).json({ message: error });
		});

};
